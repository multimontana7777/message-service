# Сервис для создания сообщений и комментариев

## Описание

Этот проект представляет собой сервис с API для создания сообщений и комментариев.

## Структура проекта

project-root/
|-- src/
| |-- modules/
| | |-- auth/
| | | |-- index.js
| | |-- comment/
| | | |-- comment.controller.js
| | | |-- comment.model.js
| | | |-- comment.schema.js
| | |-- post/
| | | |-- post.controller.js
| | | |-- post.model.js
| | | |-- post.schema.js
|-- tests/
| |-- posts/
| | |-- comment.test.js
| | |-- post.test.js
|-- .env.dev
|-- .env.dev
|-- .gitignore
|-- package.json
|-- README.md
|-- db.sql
|-- jwt_secret.key


## Инструкция по запуску приложения

1. Убедитесь, что у вас установлен Node.js и npm.
2. Установите зависимости проекта:

```bash
npm install

Создайте базу данных с использованием файла db.sql:

psql -U your_user -d your_database -a -f db.sql

Запустите приложение:

npm start

Перейдите по адресу http://localhost:3000 в вашем браузере.
SQL файл для создания базы данных
Файл db.sql содержит SQL-скрипт для создания базы данных. Вы можете выполнить его с использованием утилиты psql:


psql -U your_user -d your_database -a -f db.sql


Ключ для подписи JWT
Ключ для подписи JWT хранится в файле jwt_secret.key. Убедитесь, что вы обеспечиваете безопасное хранение этого ключа.

