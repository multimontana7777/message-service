const path = require('path');
const db = require(path.join(__dirname, '../../../', '/db'));

const createPost = async (groupId, text, createdBy) => {
    const query = 'INSERT INTO posts (group_id, text, created_by) VALUES ($1, $2, $3) RETURNING *';
    const values = [groupId, text, createdBy];
    const result = await db.one(query, values);
    return mapPostDto(result);
};

const getAllPosts = async (groupId) => {
    const query = 'SELECT p.*, COUNT(c.id) AS comments_count FROM posts p LEFT JOIN comments c ON p.id = c.post_id WHERE p.group_id = $1 GROUP BY p.id';
    const values = [groupId];
    const posts = await db.any(query, values);
    return posts.map(mapPostDto);
};

const mapPostDto = (post) => ({
    id: post.id,
    text: post.text,
    createdBy: post.created_by,
    commentsCount: parseInt(post.comments_count) || 0,
});

module.exports = { createPost, getAllPosts };
