const Joi = require('joi');

const postSchema = Joi.object({
    groupId: Joi.number().required(),
    text: Joi.string().required(),
});

module.exports = postSchema;
