const { createPost, getAllPosts } = require(`${__dirname}/post.model`);
const postSchema = require(`${__dirname}/post.schema`);

const createPostController = async (req, res) => {
    try {
        const { error } = postSchema.validate(req.body);
        if (error) {
            return res.status(400).json({ message: error.details[0].message });
        }
        
        const createdBy = req.user.id;
        const post = await createPost(req.body.groupId, req.body.text, createdBy);
        res.json(post);
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal Server Error' });
    }
};

const getAllPostsController = async (req, res) => {
    try {
        const { error } = postSchema.validate(req.body);
        if (error) {
            return res.status(400).json({ message: error.details[0].message });
        }
        
        const posts = await getAllPosts(req.body.groupId);
        res.json(posts);
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal Server Error' });
    }
};

module.exports = { createPostController, getAllPostsController };
