const path = require('path');
const db = require(path.join(__dirname, '../../../', '/db'));

const createComment = async (postId, text, createdBy) => {
    const query = 'INSERT INTO comments (post_id, text, created_by) VALUES ($1, $2, $3) RETURNING *';
    const values = [postId, text, createdBy];
    const result = await db.one(query, values);
    return mapCommentDto(result);
};

const getCommentsByPostId = async (postId) => {
    const query = 'SELECT * FROM comments WHERE post_id = $1';
    const values = [postId];
    return await db.any(query, values);
};

const mapCommentDto = (comment) => ({
    id: comment.id,
    text: comment.text,
    createdBy: comment.created_by,
});

module.exports = { createComment, getCommentsByPostId };
