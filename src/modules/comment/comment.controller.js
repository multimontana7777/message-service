const { createComment, getCommentsByPostId } = require(`${__dirname}/comment.model`);
const commentSchema = require(`${__dirname}/comment.schema`);

const createCommentController = async (req, res) => {
    try {
        const { error } = commentSchema.validate(req.body);
        if (error) {
            return res.status(400).json({ message: error.details[0].message });
        }
        
        const createdBy = req.user.id;
        const comment = await createComment(req.body.postId, req.body.text, createdBy);
        res.json(comment);
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal Server Error' });
    }
};

const getCommentsByPostIdController = async (req, res) => {
    try {
        const { error } = commentSchema.validate(req.body);
        if (error) {
            return res.status(400).json({ message: error.details[0].message });
        }
        
        const comments = await getCommentsByPostId(req.body.postId);
        res.json(comments);
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal Server Error' });
    }
};

module.exports = { createCommentController, getCommentsByPostIdController };
