const Joi = require('joi');

const commentSchema = Joi.object({
    postId: Joi.number().required(),
    text: Joi.string().required(),
});

module.exports = commentSchema;
