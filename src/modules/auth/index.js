require('dotenv').config();
const jwt = require('jsonwebtoken');
const {isJwtExpired} = require('jwt-check-expiration');

const secretKey = process.env.SECRET_KEY || 'NAUTILIUS';
const expiresIn = process.env.EXPITES_IN; // Время жизни основного токена
const refreshExpiresIn = process.env.REFRESH_EXPIRES_IN; // Время жизни рефреш токена

const createToken = (userId, groups) => {
    const payload = { id: userId, groups };
    const options = { expiresIn };
    const token = jwt.sign(payload, secretKey, options);
    const refreshToken = createRefreshToken(userId, groups);
    return { token, refreshToken };
};

const createRefreshToken = (userId, groups) => {
    const payload = { id: userId, groups };
    
    if (isJwtExpired(refreshExpiresIn)) {
        return false;
    }
    
    const options = { expiresIn: refreshExpiresIn };
    return jwt.sign(payload, secretKey, options);
};

const authenticateToken = (req, res, next) => {
    const token = req.header('Authorization');
    if (!token) return res.status(401).json({ message: 'Unauthorized' });
    
    jwt.verify(token, secretKey, (err, user) => {
        if (err) return res.status(403).json({ message: 'Forbidden' });
        
        req.user = user;
        next();
    });
};

const validateRefreshToken = (refreshToken) => {
    try {
        jwt.verify(refreshToken, secretKey);
        return true;
    } catch (err) {
        return false;
    }
};

const generateNewAccessToken = (userId, groups) => {
    const { token, refreshToken } = createToken(userId, groups);
    return { token, newRefreshToken: refreshToken };
};

module.exports = { createToken, authenticateToken, validateRefreshToken, generateNewAccessToken };
