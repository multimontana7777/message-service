require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const { createPostController, getAllPostsController } = require(`${__dirname}/modules/post/post.controller`);
const { createCommentController, getCommentsByPostIdController } = require(`${__dirname}/modules/comment/comment.controller`);
const { authenticateToken } = require(`${__dirname}/modules/auth`);

const app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.json());

app.post('/posts', authenticateToken, createPostController);
app.get('/posts', authenticateToken, getAllPostsController);
app.post('/comments', authenticateToken, createCommentController);
app.get('/comments', authenticateToken, getCommentsByPostIdController);

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
