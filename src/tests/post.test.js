const request = require('supertest');
const app = require(`${__dirname}/src/app`);

describe('Posts API', () => {
    const userToken = 'YOUR_VALID_JWT_TOKEN';
    
    it('should create a new post', async () => {
        const response = await request(app)
        .post('/posts')
        .set('Authorization', `Bearer ${userToken}`)
        .send({ groupId: 1, text: 'Test post' });
        
        expect(response.status).toBe(200);
        expect(response.body).toHaveProperty('id');
        expect(response.body).toHaveProperty('text', 'Test post');
        expect(response.body).toHaveProperty('createdBy');
        expect(response.body).toHaveProperty('commentsCount', 0);
    });
    
    it('should get all posts', async () => {
        const response = await request(app)
        .get('/posts')
        .set('Authorization', `Bearer ${userToken}`)
        .send({ groupId: 1 });
        
        expect(response.status).toBe(200);
        expect(response.body).toBeInstanceOf(Array);
    });
});
