const request = require('supertest');
const app = require(`${__dirname}/src/app`);

describe('Comments API', () => {
    const userToken = 'YOUR_VALID_JWT_TOKEN';
    
    it('should create a new comment', async () => {
        // Предполагается, что postId существует и его можно использовать для создания комментария
        const postId = 1;
        
        const response = await request(app)
        .post('/comments')
        .set('Authorization', `Bearer ${userToken}`)
        .send({ postId, text: 'Test comment' });
        
        expect(response.status).toBe(200);
        expect(response.body).toHaveProperty('id');
        expect(response.body).toHaveProperty('text', 'Test comment');
        expect(response.body).toHaveProperty('createdBy');
    });
    
    it('should get comments by postId', async () => {
        // Предполагается, что postId существует и у него есть комментарии
        const postId = 1;
        
        const response = await request(app)
        .get('/comments')
        .set('Authorization', `Bearer ${userToken}`)
        .send({ postId });
        
        expect(response.status).toBe(200);
        expect(response.body).toBeInstanceOf(Array);
    });
});
